FROM golang:1.15.2-buster

RUN go get github.com/stamblerre/gocode
RUN mv /go/bin/gocode /go/bin/gocode-gomod

RUN go get github.com/mdempsky/gocode
RUN go get github.com/uudashr/gopkgs/v2/cmd/gopkgs
RUN go get github.com/ramya-rao-a/go-outline
RUN go get github.com/acroca/go-symbols
RUN go get golang.org/x/tools/cmd/guru
RUN go get golang.org/x/tools/cmd/gorename
RUN go get github.com/cweill/gotests
RUN go get github.com/fatih/gomodifytags
RUN go get github.com/josharian/impl
RUN go get github.com/davidrjenni/reftools/cmd/fillstruct
RUN go get github.com/haya14busa/goplay/cmd/goplay
RUN go get github.com/godoctor/godoctor
RUN go get github.com/go-delve/delve/cmd/dlv
RUN go get github.com/rogpeppe/godef
RUN go get golang.org/x/tools/cmd/goimports
RUN go get golang.org/x/lint/golint